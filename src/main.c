#include <stdio.h>


#include "mem_internals.h"
#include "mem.h"
#include <assert.h>

#define HEAP_SIZE 1024
#define INITITAL_HEAP 0
#define MALLOC_NUM_FOR_TEST_EXPANSION 10000

void new_line(){
    printf("\n");
}

void test_allocation() {
    printf("Test: allocation");
    new_line();
    void *heap = heap_init(INITITAL_HEAP);
    debug_heap(stdout, heap);
    void *mem1 = _malloc(0);
    assert(mem1 != NULL && "Block 1 allocation failed");
    debug_heap(stdout, heap);

    _free(mem1);
    debug_heap(stdout, heap);
    printf("Allocation test finish with success");
    new_line();new_line();
}
static void test_single_block_release() {
    printf("Test: free one block");
    new_line();
    void* heap = heap_init(INITITAL_HEAP);
    debug_heap(stdout, heap);
    void* mem1 = _malloc(64);
    assert(mem1 != NULL && "Error: Failed to allocate single block");
    debug_heap(stdout, heap);
    _free(mem1);
    printf("Success: Single block released");
    new_line();new_line();
}
static void test_multiple_blocks_release() {
    void* heap = heap_init(INITITAL_HEAP);
    printf("Test: verifying Release of Multiple Memory Blocks");
    new_line();
    debug_heap(stdout, heap);
    void* mem1 = _malloc(64);
    void* mem2 = _malloc(128);
    void* mem3 = _malloc(256);
    assert(mem1 != NULL && "Error: Failed to allocate first block");
    assert(mem2 != NULL && "Error: Failed to allocate second block");
    assert(mem3 != NULL && "Error: Failed to allocate third block");
    debug_heap(stdout, heap);
    _free(mem1);
    _free(mem2);
    _free(mem3);
    printf("Success: Multiple blocks released");
    new_line();new_line();
}
void test_region_extension() {
    printf("Test: region extension");new_line();
    void *heap = heap_init(28);
    assert(heap != NULL && "Error: heap initialization failed");

    void *allocated_block = _malloc(100);
    assert(allocated_block != NULL && "Block allocation failed");

    heap_term();
    printf("Success: region extension");new_line();new_line();
}

void test_memory_expansion_with_address_limitation() {
    printf("Test: memory expansion with address limitation");new_line();
    void *heap = heap_init(INITITAL_HEAP);
    void *mem1 = _malloc(MALLOC_NUM_FOR_TEST_EXPANSION);
    void *mem2 = _malloc(MALLOC_NUM_FOR_TEST_EXPANSION);

    assert(mem1 != NULL && "Error: Failed to allocate first block");
    assert(mem2 != NULL && "Error: Failed to allocate second block");
    debug_heap(stdout, heap);
    _free(mem1);
    void *mem3 = _malloc(MALLOC_NUM_FOR_TEST_EXPANSION * 10);
    assert(mem3 != NULL && "Error: Failed to allocate third block");
    debug_heap(stdout, heap);

    _free(mem2);
    _free(mem3);
    debug_heap(stdout, heap);
    printf("Success: memory expansion with address limitation");new_line();
}


int main() {
    heap_init(HEAP_SIZE);
    test_allocation();
    test_single_block_release();
    test_multiple_blocks_release();
    test_region_extension();
    test_memory_expansion_with_address_limitation();
    heap_term();
    return 0;
}
